//
//  TaskModel.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit
import RealmSwift
enum TasksOrder: String {
    case asc = "asc"
    case desc = "desc"
}

enum TasksOrderName: String, CaseIterable{
    case title = "Title"
    case priority = "Priority"
    case date = "Date"
    
    var requestString: String{
        switch self {
        case .title:
            return "title"
        case .priority:
            return "priority"
        default:
            return "dueBy"
        }
    }
}

enum TasksPriority: String, CaseIterable{
    case low = "Low"
    case normal = "Normal"
    case high = "High"
    
    var intVal: Int{
        switch self {
        case .low:
            return 2
        case .normal:
            return 1
        case .high:
            return 0
        }
    }
    
    static func priorityWithInt(value:Int) -> TasksPriority{
        switch value {
        case 2:
            return .low
        case 1:
            return .normal
        default:
            return .high
        }
    }
}

class TaskModel:Object {

    @objc dynamic var id = -1
    @objc dynamic var title = ""
    @objc dynamic var dueBy: TimeInterval = -1
    @objc dynamic var priority = ""
    
    var date: Date{
        return Date(timeIntervalSince1970: dueBy)
    }
    
    convenience required init(json: [String : Any]){
        self.init()
        if let title = json["title"] as? String{
            self.title = title
        }
        
        if let priority = json["priority"] as? String{
            self.priority = priority
        }
        
        if let id = json["id"] as? Int{
            self.id = id
        }
        
        if let dueBy = json["dueBy"] as? TimeInterval{
            self.dueBy = dueBy
        }
    }
    
    func json() -> [String : Any]{
        var json = [String : Any]()
        json["id"] = self.id
        json["title"] = self.title
        json["priority"] = self.priority
        json["dueBy"] = self.dueBy
        return json
    }
    
    
}
