//
//  tokenModel.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit

class TokenModel {
    
    var token = ""
    
    init(json: [String : Any]){
        if let token = json["token"] as? String{
            self.token = token
        }
    }
    
}
