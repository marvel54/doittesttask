//
//  ReminderManager.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit
import UserNotifications

enum ReminderType:String, CaseIterable{
    case never = "Never"
    case fiveMinutes = "5 min before"
    case tenMinutes = "10 min before"
    case halfHour = "30 min before"
    case hour = "1 hour before"
    
    var interval: TimeInterval {
        switch self {
        case .fiveMinutes:
            return 300
        case .tenMinutes:
            return 600
        case .halfHour:
            return 1800
        case .hour:
            return 3600
        default:
            return 0
        }
    }
}

class ReminderManager: NSObject {
    static let shared = ReminderManager()
    
    private let notificationCenter = UNUserNotificationCenter.current()
    static private let kReminderType = "kReminderType"
    
    private override init() {
        super.init()
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        notificationCenter.requestAuthorization(options: options) {(didAllow, error) in
        }
        notificationCenter.delegate = self
    }
    
    func clearBadges(){
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func scheduleNotification(text: String, date: Date, reminderType: ReminderType, task: TaskModel) {
        self.removeReminderForTask(task)
        if reminderType != .never{
            let content = UNMutableNotificationContent()
            
            content.title = "Doit"
            content.body = "text"
            content.sound = UNNotificationSound.default
            content.badge = 1
            content.userInfo = [ReminderManager.kReminderType : reminderType.rawValue]
            
            
            let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: Date(timeIntervalSince1970: date.timeIntervalSince1970 - reminderType.interval))
            let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
            
            let identifier = "\(task.id)"
            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
            
            notificationCenter.add(request, withCompletionHandler: nil)
        }
    }
    
    func removeReminderForTask(_ task: TaskModel){
        notificationCenter.removePendingNotificationRequests(withIdentifiers: ["\(task.id)"])
    }
    
    func getReminderType(task: TaskModel, completion: @escaping (ReminderType?)->Void){
        notificationCenter.getPendingNotificationRequests { (requests) in
            DispatchQueue.main.async {
                for request in requests{
                    if request.identifier == "\(task.id)",
                    let reminderType = request.content.userInfo[ReminderManager.kReminderType] as? String{
                        completion(ReminderType(rawValue: reminderType))
                        return
                    }
                }
                completion(nil)
            }
        }
    }
}

extension ReminderManager: UNUserNotificationCenterDelegate {
    
}
