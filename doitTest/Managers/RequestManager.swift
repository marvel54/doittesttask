//
//  RequestManager.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit
import Alamofire

class RequestManager {

    private static let apiLink = "http://testapi.doitserver.in.ua/api/"
    
    private static let apiMethodAuth = apiLink + "auth"
    private static let apiMethodRegister = apiLink + "users"
    private static let apiMethodTasks = apiLink + "tasks"
    
    static let shared = RequestManager()
    
    private init() {
        
    }
    
    func updateTask(taskId: Int, parameters: Parameters, completion: @escaping (TaskModel?, Bool)->Void){
        guard let token = MainManager.shared.token else {
            completion(nil, false)
            return
        }
        
        let headers = ["Authorization": "Bearer \(token)"] as HTTPHeaders
        
        let url = RequestManager.apiMethodTasks + "/\(taskId)"
        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
            guard response.result.isSuccess else {
                print("Error while fetching data: \(String(describing: response.result.error))")
                completion(nil, false)
                return
            }
            
            if (200..<300).contains(response.response!.statusCode){
                print("******************** UPDATE TASK **********************")
                print(response.request ?? "")
                completion(TaskModel(json: parameters), true)
                return
            }
            completion(nil, false)
        }
    }
    
    func deleteTask(taskId: Int, completion: @escaping (Bool)->Void){
        guard let token = MainManager.shared.token else {
            completion(false)
            return
        }
        
        let headers = ["Authorization": "Bearer \(token)"] as HTTPHeaders
        
        let url = RequestManager.apiMethodTasks + "/\(taskId)"
        Alamofire.request(url, method: .delete, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
            guard response.result.isSuccess else {
                print("Error while fetching data: \(String(describing: response.result.error))")
                completion(false)
                return
            }
            
            if (200..<300).contains(response.response!.statusCode){
                print("******************** DELETE TASK **********************")
                print(response.request ?? "")
                completion( true)
                return
            }
            completion(false)
        }
    }
    
    func createTask(parameters: Parameters, completion: @escaping (TaskModel?, Bool)->Void){
        guard let token = MainManager.shared.token else {
            completion(nil, false)
            return
        }
        
        let headers = ["Authorization": "Bearer \(token)"] as HTTPHeaders
        
        let url = RequestManager.apiMethodTasks
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
            guard response.result.isSuccess else {
                print("Error while fetching data: \(String(describing: response.result.error))")
                completion(nil, false)
                return
            }
            if let result = response.result.value, let json = result as? [String : Any], let task = json["task"] as? [String : Any], (200..<300).contains(response.response!.statusCode){
                print("******************** CREATE TASK **********************")
                print(response.request ?? "")
                completion(TaskModel(json: task), true)
                return
            }
            completion(nil, false)
        }
    }
    
    func tasks(page: Int, order: TasksOrder, orderBy: TasksOrderName, completion: @escaping (TasksResponse?, Bool)->Void){
        guard let token = MainManager.shared.token else {
            completion(nil, false)
            return
        }
        
        let parameters = ["page" : page,
                          "sort" : "\(orderBy.requestString) \(order)"] as Parameters
        
        let headers = ["Authorization": "Bearer \(token)"] as HTTPHeaders
        
        Alamofire.request(RequestManager.apiMethodTasks, method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
            guard response.result.isSuccess else {
                print("Error while fetching data: \(String(describing: response.result.error))")
                completion(nil, false)
                return
            }
            
            if let result = response.result.value, let json = result as? [String : Any], (200..<300).contains(response.response!.statusCode){
                print("******************** TASKS **********************")
                print(response.request ?? "")
                completion(TasksResponse(json: json), true)
                return
            }
            completion(nil, false)
        }
    }
    
    func auth(isRegistration: Bool, email: String, password: String, completion: @escaping (TokenModel?, Bool)->Void){
        let parameters = ["email" : email,
                          "password" : password] as Parameters
        
        Alamofire.request(isRegistration ? RequestManager.apiMethodRegister : RequestManager.apiMethodAuth, method: .post, parameters: parameters, encoding: URLEncoding.queryString, headers: nil).responseJSON { (response) in
            guard response.result.isSuccess else {
                print("Error while fetching data: \(String(describing: response.result.error))")
                completion(nil, false)
                return
            }
            
            if let result = response.result.value, let json = result as? [String : Any], (200..<300).contains(response.response!.statusCode){
                print("******************** AUTH **********************")
                completion(TokenModel(json: json), true)
                return
            }
            completion(nil, false)
        }
    }
    
}
