//
//  MainManager.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit

class MainManager {
    static let shared = MainManager()
    
    private static let kToken = "kToken"
    
    var token:String?{
        set{
            UserDefaults.standard.set(newValue, forKey: MainManager.kToken)
        }
        
        get{
            return UserDefaults.standard.value(forKey: MainManager.kToken) as? String
        }
    }
    
    private init() {
        
    }
}
