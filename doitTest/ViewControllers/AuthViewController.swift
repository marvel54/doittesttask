//
//  AuthViewController.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//
import SVProgressHUD
import UIKit

class AuthViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginSwitcher: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if MainManager.shared.token != nil{
            self.performSegueToTasks()
        }
    }
    
    private func performSegueToTasks(){
        self.performSegue(withIdentifier: "AuthToTasks", sender: nil)
    }
    
    @IBAction func goTapped(_ sender: Any) {
        SVProgressHUD.show()
        RequestManager.shared.auth(isRegistration: self.loginSwitcher.isOn ,email: self.loginTextField.text!, password: self.passwordTextField.text!) { (token, succes) in
            SVProgressHUD.dismiss()
            if succes{
                MainManager.shared.token = token?.token
                self.performSegueToTasks()
            }else{
                SVProgressHUD.show(withStatus: "ERROR!")
                SVProgressHUD.dismiss(withDelay: 1.5)
            }
        }
    }

}
