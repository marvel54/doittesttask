//
//  TaskEditViewController.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift

class TaskEditViewController: UITableViewController {

    var task: TaskModel?
    var datePicker = UIDatePicker()
    
    @IBOutlet weak var reminderTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var titleTextView: UITextView!
    
    lazy var toolbar:UIToolbar = { () -> UIToolbar in
        let toolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(endEditing))]
        toolbar.sizeToFit()
        return toolbar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupToolBar()
        self.setupDatePicker()
        self.tryToSetupTask()
        self.addSaveButton()
        self.setupReminderPicker()
        self.reminderTextField.text = ReminderType.never.rawValue
    }

    //MARK: Requests
    
    private func createTask(task: TaskModel){
        RequestManager.shared.createTask(parameters: task.json()) { (newTaskModel, success) in
            SVProgressHUD.dismiss()
            if success, let newTaskModel = newTaskModel{
                ReminderManager.shared.scheduleNotification(text: newTaskModel.title, date: newTaskModel.date, reminderType: ReminderType(rawValue: self.reminderTextField.text!) ?? ReminderType.never, task: newTaskModel)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.notificationTaskDidCreate), object: newTaskModel)
                self.navigationController?.popViewController(animated: true)
            }else{
                SVProgressHUD.show(withStatus: "ERROR!")
                SVProgressHUD.dismiss(withDelay: 1.5)
            }
        }
    }
    
    private func updateTask(task: TaskModel){
        RequestManager.shared.updateTask(taskId: task.id, parameters: task.json()) { (newTaskModel, success) in
            SVProgressHUD.dismiss()
            if success{
                ReminderManager.shared.scheduleNotification(text: task.title, date: task.date, reminderType: ReminderType(rawValue: self.reminderTextField.text!) ?? ReminderType.never, task: task)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.notificationTaskDidUpdate), object: newTaskModel)
                self.navigationController?.popViewController(animated: true)
            }else{
                SVProgressHUD.show(withStatus: "ERROR!")
                SVProgressHUD.dismiss(withDelay: 1.5)
            }
        }
    }
    
    //MARK: Actions
    
    @objc private func endEditing(){
        self.view.endEditing(true)
    }
    
    @objc private func saveButtonTapped(){
        let realm = try! Realm()
        try! realm.write {
            self.task?.title = self.titleTextView.text
        }
        
        guard let task = self.task else {
            return
        }
        
        SVProgressHUD.show()
        if task.id == -1 {
            self.createTask(task: task)
        }else{
            self.updateTask(task: task)
        }
        
    }
    
    @IBAction func priorityValueChanged(_ sender: UISegmentedControl) {
        self.task?.priority = TasksPriority.priorityWithInt(value: sender.selectedSegmentIndex).rawValue
    }
    
    @objc private func datePickerValueChanged(_ sender: Any) {
        self.dateTextField.text = "\(self.datePicker.date)"
        self.task?.dueBy = self.datePicker.date.timeIntervalSince1970
    }
    
    //MARK: Setups
    
    private func setupToolBar(){
        self.dateTextField.inputAccessoryView = self.toolbar
        self.titleTextView.inputAccessoryView = self.toolbar
        self.reminderTextField.inputAccessoryView = self.toolbar
    }
    
    private func addSaveButton(){
        let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveButtonTapped))
        self.navigationItem.rightBarButtonItem = saveButton
    }
    
    private func setupReminderPicker(){
        let pickerView = UIPickerView()
        self.reminderTextField.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    private func setupDatePicker(){
        self.datePicker.datePickerMode = .dateAndTime
        self.dateTextField.inputView = self.datePicker
        self.datePicker.minimumDate = Date(timeIntervalSince1970: Date().timeIntervalSince1970 + 3800)
        self.datePicker.date = self.datePicker.minimumDate!
        self.dateTextField.text = "\(self.datePicker.date)"
        self.datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
    }
    
    private func tryToSetupTask(){
        guard let task = self.task else {
            self.task = TaskModel()
            self.task?.priority = TasksPriority.normal.rawValue
            self.segmentControl.selectedSegmentIndex = TasksPriority.normal.intVal
            self.reminderTextField.text = ReminderType.never.rawValue
            self.task?.dueBy = Double(Int(self.datePicker.minimumDate!.timeIntervalSince1970))
            return
        }
        self.titleTextView.text = task.title
        self.segmentControl.selectedSegmentIndex = TasksPriority(rawValue: task.priority)?.intVal ?? 1
        if task.date.timeIntervalSince1970 > self.datePicker.minimumDate!.timeIntervalSince1970 {
            self.datePicker.date = task.date
            self.dateTextField.text = "\(self.datePicker.date)"
        }
        
        ReminderManager.shared.getReminderType(task: task) { (reminder) in
            self.reminderTextField.text = reminder?.rawValue ?? ReminderType.never.rawValue
        }
    }
    
    //MARK: TableView

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.row == 2){
            self.dateTextField.becomeFirstResponder()
        }else if(indexPath.row == 3){
            self.reminderTextField.becomeFirstResponder()
        }
    }
}

extension TaskEditViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ReminderType.allCases.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ReminderType.allCases[row].rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.reminderTextField.text = ReminderType.allCases[row].rawValue
    }
    
    
    
}
