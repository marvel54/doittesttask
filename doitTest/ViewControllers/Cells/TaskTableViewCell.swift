//
//  TaskTableViewCell.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(task: TaskModel){
        self.taskNameLabel.text = task.title
        self.priorityLabel.text = task.priority
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        self.dateLabel.text = dateFormatter.string(from: task.date)
    }
    
}
