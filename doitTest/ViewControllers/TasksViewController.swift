//
//  TasksViewController.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit
import RealmSwift
import SVProgressHUD

class TasksViewController: UIViewController {

    private let taskCellIdentifier = String(describing: TaskTableViewCell.self)
    private let taskDetailSegueIdentifier = "TasksToTaskDetail"
    private let taskEditSegueIdentifier = "tasksToEditTask"
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    var tasks = [TaskModel]()
    
    var currentPage = 1
    var order : TasksOrder = .asc
    
    var orderName : TasksOrderName = .title {
        willSet{
            if newValue == self.orderName{ // if we pressed on same value, then switch asc and desc
                self.order = self.order == .asc ? .desc : .asc
            }else{
                self.order = .asc
            }
            currentPage = 1
            self.loadTasks(orderBy: newValue)
        }
    }
    
    var isDataLoading = false
    var isDataEnd = false
    var selectedTask: TaskModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.subscribeToNotifications()
        self.loadTasks(orderBy: self.orderName)
        self.setupRefreshControl()
    }
    
    //MARK: Notifications
    private func subscribeToNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(taskDidUpdate(notification:)), name: NSNotification.Name(rawValue: Constants.notificationTaskDidUpdate), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(taskDidCreate(notification:)), name: NSNotification.Name(rawValue: Constants.notificationTaskDidCreate), object: nil)
    }
    
    @objc private func taskDidUpdate(notification: Notification){
        guard let task = notification.object as? TaskModel else {
            return
        }
        for (i, item) in self.tasks.enumerated() {
            if task.id == item.id{
                self.tasks[i] = task
                self.tableView.reloadData()
                break
            }
        }
    }
    
    @objc private func taskDidCreate(notification: Notification){
        self.refreshControl.beginRefreshing()
        reloadTasks()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.notificationTaskDidUpdate), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.notificationTaskDidCreate), object: nil)
    }
    
    //MARK: Requests
    
    private func loadTasks(orderBy: TasksOrderName){
        RequestManager.shared.tasks(page: currentPage, order: order, orderBy: orderBy) { (tasks, success) in
            self.refreshControl.endRefreshing()
            guard let tasks = tasks, success else {
                let realm = try! Realm()
                self.tasks = Array(realm.objects(TaskModel.self))
                self.currentPage = 1
                self.tableView.reloadData()
                return
            }
            if(self.currentPage == 1){
                self.tasks.removeAll()
            }
            
            self.tasks.append(contentsOf: tasks.tasks)
            self.updateRealm(isNeedDeleteOldObjects:  self.currentPage == 1, newObjects: tasks.tasks)
            self.currentPage += 1
            self.isDataEnd = self.tasks.count == tasks.count
            self.isDataLoading = false
            self.tableView.reloadData()
        }
    }
    
    private func updateRealm(isNeedDeleteOldObjects: Bool, newObjects: [TaskModel]){
        let realm = try! Realm()
        try! realm.write {
            if isNeedDeleteOldObjects{
               realm.deleteAll()
            }
            for object in self.tasks{
                realm.add(object)
            }
        }

    }
    
    @objc private func reloadTasks(){
        self.currentPage = 1
        self.loadTasks(orderBy: self.orderName)
    }
    
    //MARK: Setups
    
    private func setupRefreshControl(){
        self.tableView.addSubview(self.refreshControl)
        self.refreshControl.addTarget(self, action: #selector(reloadTasks), for: .valueChanged)
        self.refreshControl.beginRefreshing()
    }
    
    private func setupTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: taskCellIdentifier, bundle: nil), forCellReuseIdentifier: taskCellIdentifier)
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let taskDetailVC = segue.destination as? TaskDetailsViewController{
            taskDetailVC.task = sender as? TaskModel
        }
    }
    
    //MARK: Actions
    
    @IBAction func orderTapped(_ sender: Any) {
        self.showOrderActionSheet()
    }
    
    @IBAction func addTapped(_ sender: Any) {
        self.performSegue(withIdentifier: taskEditSegueIdentifier, sender: nil)
    }
    
    //MARK: Helpers
    private func showOrderActionSheet(){
        let alertController = UIAlertController(title: "Order By", message: nil, preferredStyle: .actionSheet)
        
        for orderValue in TasksOrderName.allCases{ // add all order cases
            let action = UIAlertAction(title: orderValue.rawValue, style: .default, handler: { (action) in
                self.orderName = orderValue
                alertController.dismiss(animated: true, completion: nil)
            })
            action.setValue(self.orderName == orderValue, forKey: "checked")
            alertController.addAction(action)
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
            //Cancel
            alertController.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension TasksViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: taskCellIdentifier, for: indexPath)
        if let cell = cell as? TaskTableViewCell{
            cell.setup(task: self.tasks[indexPath.row])
        }
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            RequestManager.shared.deleteTask(taskId: self.tasks[indexPath.row].id) { (success) in
                if(success){
                    ReminderManager.shared.removeReminderForTask(self.tasks[indexPath.row])
                    let realm = try! Realm()
                    try! realm.write {
                        realm.delete(self.tasks[indexPath.row])
                    }
                    tableView.beginUpdates()
                    self.tasks.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                    tableView.endUpdates()
                }else{
                    SVProgressHUD.show(withStatus: "ERROR!")
                    SVProgressHUD.dismiss(withDelay: 1.5)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: taskDetailSegueIdentifier, sender: self.tasks[indexPath.row])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        if(h >= scrollView.bounds.height && y > h && !self.isDataLoading && self.currentPage > 1 && !self.isDataEnd){
            self.isDataLoading = true
            self.loadTasks(orderBy: self.orderName)
        }
    }
}
