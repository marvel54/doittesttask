//
//  TaskDetailsViewController.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit

class TaskDetailsViewController: UITableViewController {

    @IBOutlet weak var taskTitleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var priorityLabel: UILabel!
    @IBOutlet weak var reminderLabel: UILabel!
    
    var task: TaskModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTask()
        self.addEditButton()
        self.subscribeToNotifications()
    }
    
    //MARK: Notifications
    
    private func subscribeToNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(taskDidUpdate(notification:)), name: NSNotification.Name(rawValue: Constants.notificationTaskDidUpdate), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.notificationTaskDidUpdate), object: nil)
    }
    
    @objc private func taskDidUpdate(notification: Notification){
        guard let task = notification.object as? TaskModel else {
            return
        }
        self.task = task
        self.setupTask()
    }
    
   //MARK: Actions
    @objc private func editButtonTapped(){
        self.performSegue(withIdentifier: "DetailToEdit", sender: nil)
    }
    
    //MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let taskEditVC = segue.destination as? TaskEditViewController{
            taskEditVC.task = self.task
        }
    }
    
    //MARK: Setups
    private func addEditButton(){
        let editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editButtonTapped))
        self.navigationItem.rightBarButtonItem = editButton
    }
    
    private func setupTask(){
        guard let task = self.task else { return }
        self.taskTitleLabel.text = task.title
        self.priorityLabel.text = task.priority
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE d MMM, yyyy"
        self.dateLabel.text = dateFormatter.string(from: task.date)
        
        ReminderManager.shared.getReminderType(task: task) { (reminder) in
            self.reminderLabel.text = reminder?.rawValue ?? ReminderType.never.rawValue
        }
    }

    //MARK: TableView
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
