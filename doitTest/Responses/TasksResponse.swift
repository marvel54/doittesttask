//
//  TasksResponse.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit

class TasksResponse {
    
    var current = -1
    var limit = -1
    var count = -1
    
    var tasks = [TaskModel]()
    
    init(json: [String : Any]){
        self.setupMeta(json: json)
        self.setupTasks(json: json)
    }
    
    private func setupTasks(json: [String: Any]){
        if let tasks = json["tasks"] as? [[String : Any]]{
            for task in tasks{
                self.tasks.append(TaskModel(json: task))
            }
        }
    }
    
    private func setupMeta(json: [String: Any]){
        if let meta = json["meta"] as? [String : Any]{
            if let current = meta["current"] as? Int{
                self.current = current
            }
            
            if let limit = meta["limit"] as? Int{
                self.limit = limit
            }
            
            if let count = meta["count"] as? Int{
                self.count = count
            }
        }
    }
}
