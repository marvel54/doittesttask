//
//  Constants.swift
//  doitTest
//
//  Created by VasylChekun on 14.02.19.
//  Copyright © 2019 VasylChekun. All rights reserved.
//

import UIKit

class Constants {
    static let notificationTaskDidUpdate = "notificationTaskDidUpdate"
    static let notificationTaskDidCreate = "notificationTaskDidCreate"
}
